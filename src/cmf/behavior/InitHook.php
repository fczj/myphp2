<?php
// +---------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +---------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +---------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +---------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +---------------------------------------------------------------------
namespace app\cmf\behavior;

use think\Db;
use think\facade\Hook;
class InitHook
{

    // 行为扩展的执行入口必须是run
    public function run()
    {
        //加载外部之插件的勾子
        $plugins = Db::name('hook_plugin')->field('hook,plugin')->where('status', 1)
            ->order('list_order ASC')
            ->select();
       //var_dump($plugins);
        if (!empty($plugins)) {
            foreach ($plugins as $hookPlugin) {
                Hook::add($hookPlugin['hook'], cmf_get_plugin_class($hookPlugin['plugin']));
            }
        }
    }
}