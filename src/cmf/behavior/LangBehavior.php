<?php
// +---------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +---------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +---------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +---------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +---------------------------------------------------------------------
namespace app\cmf\behavior;

use think\facade\Lang;
use think\facade\Request;


class LangBehavior
{

    // 行为扩展的执行入口必须是run
    public function run()
    {
       // dump('liyis=>勾子LangBehavior被执行'.APP_ROOT);
        $request = Request::instance();
        $langSet = $request->langset();
       //echo  CMF_PATH. 'lang' . DS . $langSet . EXT;
        Lang::load([
            
            
            CMF_PATH. 'lang' . DS . $langSet . EXT
        ]);

 
        // 加载应用公共语言包
       
        //"/www/wwwroot/678.8842021.com/public/../app/admin/lang/zh-cn/common.php"
        //"/www/wwwroot/678.8842021.com/public/../app/user/lang/zh-cn/common.php"
        // "/www/wwwroot/678.8842021.com/public/../app/web/lang/zh-cn/common.php"
        // "/www/wwwroot/678.8842021.com/public/../app/api/lang/zh-cn/common.php"
        $apps = cmf_scan_dir(APP_PATH . '*', GLOB_ONLYDIR);
        foreach ($apps as $app) {
            //dump(APP_PATH . $app . DS . 'lang' . DS . $langSet . DS . 'common' . EXT);
            Lang::load([
                APP_PATH . $app . DS . 'lang' . DS . $langSet . DS . 'common' . EXT,
            ]);
        }

    }
}