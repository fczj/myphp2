<?php
// +----------------------------------------------------------------------javascript:;
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 浮尘 < 13456789@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\cmf\controller\AdminBase;
use think\Db;

class Liyi extends AdminBase
{
    /**
     * 后台菜单管理
     * @adminMenu(
     *     'name'   => '后台菜单',
     *     'parent' => 'admin/Setting/default',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '后台菜单管理',
     *     'param'  => ''
     * )
     */
    static $msg=""; 
    public function index()
    {
     
             $pid =$this->request->param('parent_id'); 
             $mid =$this->request->param('menu_id'); 
             if(!empty($pid))
             {
              
        $result = Db::name('AdminMenu')->where(['id'=>$pid])->find();
                           //模块名
        $appname =$result['app']; 
          
        //控制器名
        $controllername =$result['controller'];
        
        $actionname=$result['action']; 
             }else 
             {
                    //模块名
        $appname =$this->request->param('app'); 
          
        //控制器名
        $controllername =$this->request->param('controller'); 
        $actionname=$this->request->param('acition'); 
        
             
        }
        if(empty($appname) || empty($controllername) )
        {
           $this->error("请提供app-controller");
        }
         if(empty($actionname))
         {
             $actionname="index";
         }
        $config=    Db::getConfig();
        $databasename=$config["database"];
         $tables=Db::query('show tables  from `'.$databasename.'`');
         $mytables=[];
           foreach ($tables as $table )
       {
          
           $itemdata['key']=$table['Tables_in_'.$databasename];
          array_push($mytables,$itemdata);
       }
       $this->assign('modulename',$appname);
       $this->assign('controllername',$controllername);
      $this->assign('actionname',$actionname);
       $this->assign('mytables',$mytables);
           // dump($result);
      
     return $this->fetch();
    }
    
         //ajax方法获取表中的有字段名
       public function getfields()
       {
             
           $var =$this->request->param('tablename'); 
           if(empty($var))
           {
               $this->error("请提供表名");
           }
        
            $result=Db::query('show full columns from '.$var);
            
            $list=[];
     
       foreach ($result as $item )
       {
          
           $itemdata['key']=$item['Field'];
          array_push($list,$itemdata);
       }
      
           return json($result);
          
       }

            
  /**
   * 
   * 创建模型，控制器，动作的目录结构      
   * 
   */  
 private   function createFolder($app,$controller,$action){
  
     
  
      $modulePath= env('app_PATH').$app; //指向app\$app模块名
      $conPath=$modulePath.'/controller';//app\$app模块名\controller
      $modelPath=$modulePath.'/model';//app\$app模块名\model
      $behaviorPath=$modulePath.'/behavior';  //app\$app模块名\behavior
      $viewPath=$modulePath.'/view';  //app\$app模块名\view
      $viewSubPath=$viewPath.'/'.$controller;  //app\$app模块名\view\$controller
      $publicPath=$viewPath.'/public';  //app\$app模块名\view\public

      $controllerFile=$conPath.'/'.ucfirst($controller).'.php'; //app\$app模块名\controller\$controller.php
      $actionFile=$viewSubPath.'/'.strtolower($action).'.html';////app\$app模块名\view\$controller\$action.html
     //$headerfilePath=$publicPath.'/header.html';//头文件html
      
      //创建module目录(app\module名)
      if(!file_exists($modulePath)){
         mkdir($modulePath,0777,true);
      }
      //创建controller目录(app\module名称\controller)
      if(!file_exists($conPath)){
      
           mkdir($conPath,0777,true);
      }
       //创建model目录(app\module名称\model)
        if(!file_exists($modelPath)){
         mkdir($modelPath,0777,true);
      }
     //创建behavior目录(app\module名称\behavior)
        if(!file_exists($behaviorPath)){
         mkdir($behaviorPath,0777,true);
      }
       //创建view目录(app\module名称\view)
        if(!file_exists($viewPath)){
         mkdir($viewPath,0777,true);
      }
      
       //创建view目录下与控制器同名的目录(app\module名称\view\控制器名称)
          if(!file_exists($viewSubPath)){
         mkdir($viewSubPath,0777,true);
      }
       //创建视图下的public目录(app\module名称\view\public)
       if(!file_exists($publicPath)){
         mkdir($publicPath,0777,true);
      }
   
        return true;
 }

 //创建表
    public function createTable()
    {
        if(!$this->request->isPost()) {
            $this->error("非法请求!");
        }
        $params=request()->param();
        $fields=$params["fields"];
        $fieldinc=$params["fieldsinc"];
        $fieldsindex=$params["fieldsindex"];
        $fields=substr($fields,0,strlen($fields)-1);
        $tbname=$params["tbname"];
        if(config("database.prefix"))
        {
            $tbname=  config("database.prefix").$tbname;
        }

        $sqlstr="CREATE TABLE `".$tbname."` (".$fields;
        $incstr="";
        $ar=explode('|',$fieldinc);
        if($ar&&$ar[0]){
            //如果存在自动增长的列，设置增量
            $incstr=" AUTO_INCREMENT=1 ";
            $sqlstr .=", PRIMARY KEY (`".$ar[0]."`)";

        }
        //创建索引
        //$fieldinc
       /* $ar2=explode('@@',$fieldsindex);
        foreach ($ar2 as $item )
        {
            if($item != null && strlen($item) >0) {
                //UNIQUE INDEX(height)
                $ar3 = explode("|", $item);
                $sqlstr .= "," . $ar3[1] . " INDEX(" . $ar3[0] . ")";
            }
        }*/
        $sqlstr .="  )engine=innodb ".$incstr." charset=utf8mb4;";
        $isexist="SELECT COUNT(*) FROM information_schema.TABLES WHERE table_name =`".$tbname."`";
        if($isexist>0)
        {


            $ab= Db::execute("DROP table IF EXISTS `".$tbname."`");
        }

        $ab= Db::execute($sqlstr);
        $at=3;
    }

    
         //列表查询页面
        //post参数：
        //app=模块名&controller=控制器名=action=操作名&fields=字段1@@@字段2@@@字段3@@@
       public function buildPage()
       {
           if(!$this->request->isPost()) {
            $this->error("非法请求!");
           }
                $deldir =$this->request->param("deldir");

                $app=$this->request->param("app");
                $controller   = $this->request->param("controller");
                $action=$this->request->param("action");
                $fieldArr=trim($this->request->param("fields"),'@@@');
                $tablefullname=$this->request->param("tbname");
                $tabname=$this->request->param("tbname");
                $keyword=input("keyword");
                $sord=input("sord");
                if( empty($app) || empty($controller) || empty($action) || empty($fieldArr) || empty($tabname))
                {
                    $this->error("请提供(模型[$app]-控制器[$controller]-操作[$action]-字段[$fieldArr]-表[$tabname])参数！");
                }
                if(empty($sord) || empty($keyword))
                {
                     $this->error("请提供复合搜索字段和查询排序字段");
                }
           
               $strkeyword=trim(str_replace(',','|',$keyword),'|');
               Liyi::$msg ='';
               //删除应用目录
               if($deldir=="1") {
                  // $modulePath= env('app_PATH').$app; //指向app\$app模块名
                   //创建module目录(app\module名)
                  // deldir($modulePath);

               }

                $this->createFolder($app, $controller, $action);
                $this->createAddHtmlFile($app,$controller,$action,$tabname,$fieldArr);
                $this->createEditHtmlFile($app,$controller,$action,$tabname,$fieldArr);
                $this->createControllerFile($app,$controller,$action,$tabname,$strkeyword,$sord);
                $this->createIndexHtmlFile($app,$controller,$action,$tabname,$fieldArr);
                $this->createPubHeaderFile($app,$controller,$action);
                $this->success(Liyi::$msg,url($app.'/'.$controller.'/'.$action));
           
          
 
      
       }

    public function deldir($dir) {

             //先删除目录下的文件：

        $dh = opendir($dir);

        while ($file = readdir($dh)) {

            if($file != "." && $file!="..") {

                $fullpath = $dir."/".$file;

                if(!is_dir($fullpath)) {

                    unlink($fullpath);

                } else {

                    deldir($fullpath);

                }

            }

        }

        closedir($dh);

//删除当前文件夹：

        if(rmdir($dir)) {

            return true;

        } else {

            return false;

        }

    }
          //创建edit.html
          private function createEditHtmlFile($app,$controller,$action,$table,$fieldArr)
          {
              
      $modulePath= env('app_PATH').$app; //app/$app模块目录
      $viewPath=$modulePath.'/view';  //app/$app模块/view目录
      $viewSubPath=$viewPath.'/'.$controller;  //view/$controller目录
      $editfilePath=$viewSubPath.'/edit.html';//view/$controller/add.html
    
          
          $tabledata=Db::query('show full columns from '.$table);
                
          $fieldslist=Array();//存储字段说明
          $fieldstype=Array();//存储字段类型
          $fieldsExtra=Array();//存储字段Extra
          $fieldsDefault=Array();//存储字段缺省值
          
          $itemdata=[];
   
       foreach ($tabledata as $item )
       {
           
          $fieldslist[$item['Field']] = $item['Comment']; //说明
          $fieldstype[$item['Field']] = $item['Type']; //类型
          $fieldsExtra[$item['Field']] = $item['Extra'];//额外介绍
          $fieldsDefault[$item['Field']] = $item['Default'];//缺省值
       }
      
           //数据校验
           //$validate=new AdminMenuValidate();
           //$result = $validate->check($data);
            $fieldsarr=explode('@@@',$fieldArr);
           
             
            $html="";
            $htmlhead='<include file="public/header"/>
</head>
<body>
<div class="wrap">
    <ul class="nav nav-tabs">
    <li class="active"><a>编辑</a></li>
    </ul>
    <form method="post" class="form-horizontal js-ajax-form margin-top-20" action="{:url(\''.$controller.'/editPost\')}">
   
    ';
     $htmlcontent='';
   
     foreach ($fieldsarr as $item2)
     {  
         //增量字段不纳入
         if(strpos($fieldsExtra[$item2],'increment')) continue;
         
          $label=  $fieldslist[$item2];
         if(empty($label)) $label=$item2;
            
             $htmlcontent.='
      
        <div class="form-group">
            <label for="input-score" class="col-sm-2 control-label">'.$label.'</label>
            <div class="col-md-6 col-sm-10">
                <input type="text" class="form-control" id="input-score" name="'.$item2.'" value="{$app.'.$item2.'}" />
            </div>
        </div>
     
        ';
          
     }
     /*
        <foreach $app as $key=>$value>
        <div class="form-group">
            <label for="input-score" class="col-sm-2 control-label">'.$label.'</label>
            <div class="col-md-6 col-sm-10">
                <input type="text" class="form-control" id="input-score" name="'.$item2.'" value="$value">
            </div>
        </div>
        </foreach> */
      
        $htmlend='<div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <input type="hidden" name="id" value="{$id}">
                <button type="btn_submit" id=\'btn_submit\' name=\'btn_submit\' class="btn btn-primary js-ajax-submit">{:lang(\'SAVE\')}</button>
            </div>
        </div>
    </form>
</div>
<script src="__STATIC__/js/admin.js"></script>
<script>

	$(\'#btn_submit3\').on("click",function(){
	
	    var datastr=$("#add").serialize();
           
         $.post("{:url(\'/'.$app.'/'.$controller.'/editpost\')}",datastr,function(result,b){
             debugger;
          if(b ==\'success\')
           {
              layer.msg(result.msg);
           }else 
           {
                layer.msg(result.msg);
           }
        });
    });


		return false;
	});
</script>

</body>
</html>';


     
      
   
       if(!file_exists($viewSubPath)){
         mkdir($viewSubPath,0777,true);
      }
      
    
      //创建$action动作页面内容
      //生成添加页面
 if (!file_exists($editfilePath)){
     
    
          $viewfile = fopen($editfilePath, "w");
          $html=$htmlhead.$htmlcontent.$htmlend;
        fwrite($viewfile, $html);
          fclose($viewfile);
           Liyi::$msg .=$editfilePath.'创建成功！<br />';
          return 1;
 }
  Liyi::$msg .=$editfilePath.'已经存在！<br />';
 return 2;
          }
        //创建Add.html
          private function createAddHtmlFile($app,$controller,$action,$table,$fieldArr)
          {
              
      $modulePath= env('app_PATH').$app; //app/$app模块目录
      $viewPath=$modulePath.'/view';  //app/$app模块/view目录
      $viewSubPath=$viewPath.'/'.$controller;  //view/$controller目录
      $addfilePath=$viewSubPath.'/add.html';//view/$controller/add.html
    
          $tabledata=Db::query('show full columns from '.$table);
                
          $fieldslist=Array();//存储字段说明
          $fieldstype=Array();//存储字段类型
          $fieldsExtra=Array();//存储字段Extra
          $fieldsDefault=Array();//存储字段缺省值
          
          $itemdata=[];
   
       foreach ($tabledata as $item )
       {
           
          $fieldslist[$item['Field']] = $item['Comment']; //说明
          $fieldstype[$item['Field']] = $item['Type']; //类型
          $fieldsExtra[$item['Field']] = $item['Extra'];//额外介绍
          $fieldsDefault[$item['Field']] = $item['Default'];//缺省值
       }
      
           //数据校验
           //$validate=new AdminMenuValidate();
           //$result = $validate->check($data);
            $fieldsarr=explode('@@@',$fieldArr);
           
             
            $html="";
            $htmlhead='<include file="public/header"/>
</head>
<body>
<div class="wrap" style="height: 300px;">
    <form  class="form-horizontal js-ajax-form margin-top-20" id="add" action="">
      <div class="form-group">
          <label for="input-score" class="col-sm-2 control-label">页面地址</label>
            <div class="col-md-6 col-sm-10">
               {:Request::url(true)}
            </div>
        </div>
    ';
     $htmlcontent='';
   
     foreach ($fieldsarr as $item2)
     {  
         //增量字段不纳入
         if(strpos($fieldsExtra[$item2],'increment')) continue;
         
          $label=  $fieldslist[$item2];
         if(empty($label)) $label=$item2;
            
             $htmlcontent.='
        <div class="form-group">
            <label for="input-score" class="col-sm-2 control-label">'.$label.'</label>
            <div class="col-md-6 col-sm-10">
                <input type="text" class="form-control" id="input-score" name="'.$item2.'" value="">
            </div>
        </div>
        ';
          
     }
   
      
        $htmlend='<div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <input type="hidden" name="id" value="">
                <button type="btn_submit" id=\'btn_submit\' name=\'btn_submit\' class="btn btn-primary js-ajax-submit">{:lang(\'SAVE\')}</button>
            </div>
        </div>
    </form>
</div>
<script src="__STATIC__/js/admin.js"></script>
<script>
$(function(){
	$(\'#btn_submit\').on("click",function(){
	
	    var datastr=$("#add").serialize();
           
         $.post("{:url(\'/'.$app.'/'.$controller.'/addpost\')}",datastr,function(result,b){
             debugger;
          if(b ==\'success\')
           {
              layer.msg(result.msg);
           }else 
           {
                layer.msg(result.msg);
           }
        });
      	return false;
    });


	
	});
</script>

</body>
</html>';


     
      
   
       if(!file_exists($viewSubPath)){
         mkdir($viewSubPath,0777,true);
      }
      
    
      //创建$action动作页面内容
      //生成添加页面
 if (!file_exists($addfilePath)){
     
    
          $viewfile = fopen($addfilePath, "w");
          $html=$htmlhead.$htmlcontent.$htmlend;
        fwrite($viewfile, $html);
          fclose($viewfile);
            Liyi::$msg .=$addfilePath.'创建成功！<br />';
          return 1;
 }
   Liyi::$msg .=$addfilePath.'已经存在！<br />';
 return 2;
          }
          
      //创建Index.php
       public function createControllerFile($app,$controller,$action,$table,$strkeyword,$sord){
          
             if(strpos($table,config('database.prefix'))==0)
                {
                    $table=str_replace(config('database.prefix'),'',$table);
                   
                }
            $conPath= env('app_PATH').$app.'/controller'; //app\$app模块名\controller
           $controllerFile=$conPath.'/'.ucfirst($controller).'.php'; //app\$app模块名\controller\$controller.php
              
               if(!file_exists($conPath)){
                mkdir($conPath,0777,true);
               }
          
            //控制器文件$controller不存在,创建它
       //否则不创建，以免覆盖
       //app\module名称\controller\$controller.php
       if(!file_exists($controllerFile)){
         $contrfile = fopen($controllerFile, "w");
          //获取文档模版内容
          $txt='<?php
// +----------------------------------------------------------------------javascript:;
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 浮尘 < 13456789@qq.com>
// +----------------------------------------------------------------------
namespace app\\'.$app.'\\controller;

use app\cmf\controller\AdminBase;
use think\Db;
use app\user\model\UserModel;
use think\facade\Validate;


class '.ucfirst($controller).' extends AdminBase
{'.'
    /**
     * 后台菜单管理
     * @adminMenu(
     *     \'name\'   => \'后台菜单\',
     *     \'parent\' => \'admin/Setting/default\',
     *     \'display\'=> false,
     *     \'hasView\'=> true,
     *     \'order\'  => 10000,
     *     \'icon\'   => \'\',
     *     \'remark\' => \'后台菜单管理\',
     *     \'param\'  => \'\'
     * )
     */
     
      public function index()
    {
        $where   = [];
        $request = input(\'request.\');

        if (!empty($request[\'tuid\'])) {
            $where[\'id\'] = intval($request[\'tuid\']);
        }
        $_where = \'1=1\';
        $keywordComplex = [];
        if (!empty($request[\'keyword\'])) {
            $keyword = $request[\'keyword\'];
        ';
    
          $txt.=' $keywordComplex[\''.$strkeyword.'\']    = [\'like\', "%$keyword%"];';
         
        $txt.='}
      
        $usersQuery = Db::name(\''.$table.'\');

	
        $list = $usersQuery';
      
          $txt.='->whereOr($keywordComplex)';
          $txt.='
            ->where($where)
            ->where($_where)';
            if(!empty($sord)){
            $txt.='->order("'.$sord.' DESC")';
            }
            $txt.='->paginate(20);
      
        // 获取分页显示
        $page = $list->render();
        $this->assign(\'list\', $list);
        $this->assign(\'page\', $page);
       return $this->fetch();
    }
      public function add()
    {
     return $this->fetch();
    }
 
      //更新数据
      public function update()
    {
      $this->success("修改成功！");
    }
    //前台ajax添加记录的post方法
      public function addpost()
    {
    
       $this->success("修改成功！");
    
    }
    
     public function list()
    {
    
     return $this->fetch();
    }
    
      public function delete()
    {
        
        $id = $this->request->param(\'id\', 0, \'intval\');
        if (!empty($id)) {
            //状态：1正常，2审核中，3已删除，4官方删除
            $result = Db::name(\''.$table.'\')->where(["id" => $id])->delete();
            if ($result !== false) {
                $this->success("应用删除成功！", url("'.$controller.'/'.$action.'"));
            } else {
                $this->error(\'应用删除失败！\');
            }
        } else {
            $this->error(\'数据传入失败！\');
        }
    }

    public function edit()
    {
       
        $id = $this->request->param(\'id\', 0, \'intval\');
        $app = DB::name(\''.$table.'\')->where(["id" => $id])->find();
        $this->assign("id",$id);
        $this->assign("app",$app);
        return $this->fetch();
    }

    public function editPost()
    {
    
        if ($this->request->isPost()) {
           // if (strpos($_POST[\'img\'], \'base64\') === false && strpos($_POST[\'img\'], \'/upload\') === false) {
          //      $_POST[\'img\'] = \'/upload/\' . $_POST[\'img\'];
           // }
          
           $arr=$_POST;
            unset($arr["btn_submit"]);
            $result = DB::name(\''.$table.'\')->where("id",$arr["id"])->update($arr);
           
            if ($result !== false) {
              
                $this->success("保存成功！");
            } else {
                $this->error("保存失败！");
            }

        }
    }

      //前台ajax查询数据的post方法
     public function queryPost()
    {
     echo \'queryPost_op\';
      $this->success("查询成功！");
    }
    }';
    
          
          fwrite($contrfile, $txt);
          fclose($contrfile);
          Liyi::$msg .=$controllerFile.'创建成功！<br />';
          return 1;
       }
        Liyi::$msg .=$controllerFile.'已经存在！<br />';
       return 2;
       }       
 
      //创建index.html
      private function createIndexHtmlFile($app,$controller,$action,$table,$fieldArr)
      {
           
          //确定文件目录
          $viewfilePath= strtolower(env('app_PATH').$app.'/view'); //app\$app模块名\view
          $actionfilePath= strtolower(env('app_PATH').$app.'/view'.'/'.$controller); //app\$app模块名\view\$controller
          $actionFile=strtolower($actionfilePath.'/'.$action.'.html'); //app\$app模块名\view\$controller\$action.html
         
       
          //从数据库获取相关记录值
          $tabledata=Db::query('show full columns from '.$table);
          $fieldslist=Array();//存储字段说明
          $fieldstype=Array();//存储字段类型
          $fieldsExtra=Array();//存储字段Extra
          $fieldsDefault=Array();//存储字段缺省值
          
          $itemdata=[];

       foreach ($tabledata as $item )
       {
           
          $fieldslist[$item['Field']] = $item['Comment']; //说明
          $fieldstype[$item['Field']] = $item['Type']; //类型
          $fieldsExtra[$item['Field']] = $item['Extra'];//额外介绍
          $fieldsDefault[$item['Field']] = $item['Default'];//缺省值
       }
      
    
            //获得数组结构的字段清单
            $fieldsarr=explode('@@@',$fieldArr);
            //开始构建文档内容
            $headercontent='
           <include file="public/header"/>
</head>
<body>
  <style>
        table.table-bordered td{word-break:break-all;}
    </style>
<div class="wrap">
   <ul class="nav nav-tabs">
        <li class="tabs_li tabs_index active"><a>现有清单</a></li>
        <li class="tabs_li tabs_add"><a href="javascript:void(0)" onclick="add_op()">添加记录</a></li>
    </ul>
    <form class="well form-inline margin-top-20" method="post" action="{:url(\''.$app.'/'.$controller.'/index\')}">
        用户ID：
        <input class="form-control" type="text" name="tuid" style="width: 200px;" value="{:input(\'request.tuid\')}"
               placeholder="请输入ID">
        &nbsp;&nbsp;&nbsp;关键字：
        <input class="form-control" type="text" name="keyword" style="width: 200px;" value="{:input(\'request.keyword\')}"
               placeholder="用户名/昵称/邮箱">

	
        <input type="submit" class="btn btn-primary" value="搜索"/>
        <a class="btn btn-danger" href="{:url(\'admin/adminindex/index\')}">清空</a>
    </form>';
            
             
               $htmlcontent='
      <form method="post" class="js-ajax-form">
    
        <table class="table table-hover table-bordered">
            <thead>
            <tr>
        ';
     foreach ($fieldsarr as $item2)
     {  
         //增量字段不纳入
        // if(strpos($fieldsExtra[$item2],'increment')) continue;
         
          $label=  $fieldslist[$item2];
         if(empty($label)) $label=$item2;
            
         $htmlcontent.='  <th>'.$label.'</th>'; 
          
     }  
      $htmlcontent .='<th>操作</th></tr></thead>
      <tbody>
      <foreach name="list" item="vo">
      <tr>';
         foreach ($fieldsarr as $item2)
     {  
      
      
         $htmlcontent.='<td>{$vo.'.$item2.'}</td>'; 
          
     } 
      $htmlcontent .= '<td style="white-space:nowrap"><a  class="btn btn-primary"  href="{:url(\''.$app.'/'.$controller.'/edit\',array(\'id\'=>$vo[\'id\']))}" >{:lang(\'EDIT\')}</a> 
						<a  class="btn btn-primary" href="{:url(\''.$app.'/'.$controller.'/delete\',array(\'id\'=>$vo[\'id\']))}" >{:lang(\'DELETE\')}</a>';
	  $htmlend='</td></tr>
              </foreach>
              </tbody>
          </table>
          <div class="pagination">{$page|raw}</div>
		
      </form>
         </div>
         <script src="__STATIC__/js/admin.js"></script>
         </body>
         <script>

	
	function add_op(){
		$(\'.tabs_li\').removeClass(\'active\');
		$(\'.tabs_add\').addClass(\'active\');
		layer.open({
		  type: 2,
		  area: [\'340px\', \'360px\'],
		  fixed: false, //不固定
		  maxmin: true,
		  title:\'添加记录\',
		  content: "{:url(\''.$controller.'/add\')}",
		  end:function(){
			  $(\'.tabs_li\').removeClass(\'active\');
			  $(\'.tabs_index\').addClass(\'active\');
		  }
		});
	}
	

	
	function edit_op(obj,id){
		pid = obj.options[obj.options.selectedIndex].value;
		$.get("{:url(\'admin/adminindex/editPid\')}?id="+id+\'&pid=\'+pid,function(res){
			if(res.code==200){
				layer.msg(\'修改成功\');
			}else{
				layer.msg(\'修改失败\');
			}
		});
		
	}
	

</script>
       </html>';
        $html=$headercontent.$htmlcontent.$htmlend;
        
          if(!file_exists($actionfilePath)){
                mkdir($actionfilePath,0777,true);
               }
        if (!file_exists($actionFile)){
      
          $actfile = fopen($actionFile, "w");
        
        fwrite($actfile, $html);
          fclose($actfile);
           Liyi::$msg .=$actionFile.'创建成功！<br />';
        return 1;
      }
       Liyi::$msg .=$actionFile.'已经存在！<br />';
      return true;
      }
      
 
       //创建public/header.html
       private function createPubHeaderFile($app,$controller,$action)
       {
           $modulePath= env('app_PATH').$app; //app/$app模块目录
           $viewPath=$modulePath.'/view';  //app/$app模块/view目录
           $publicFlePath=$viewPath.'/public';//view/public目录
           $headerfilePath=$publicFlePath.'/header.html';//view/public/header.html路径
           
           //创建public目录
         if(!file_exists($publicFlePath)){
             mkdir($publicFlePath,0777,true);
          }
           
           $content='<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="__TMPL__/public/assets/themes/{:cmf_get_admin_style()}/bootstrap.min.css" rel="stylesheet">
    <link href="__TMPL__/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="__STATIC__/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="__STATIC__/css/style.css" rel="stylesheet" type="text/css">
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "__ROOT__/",
            WEB_ROOT: "__WEB_ROOT__/",
            JS_ROOT: "static/js/",
            APP: \'{$Request.module}\'/*当前应用名*/
        };
    </script>
    <script src="__TMPL__/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="__STATIC__/js/wind.js"></script>
    <script src="__TMPL__/public/assets/js/bootstrap.min.js"></script>
	<script src="__TMPL__/public/assets/simpleboot3/layer/layer.js"></script>
    <script>
        Wind.css(\'artDialog\');
        Wind.css(\'layer\');
        $(function () {
            $("[data-toggle=\'tooltip\']").tooltip();
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
  
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
   ';
    //创建header头文件内容
      //若存在，不创建
        if(!file_exists($headerfilePath)){
            $headerfile = fopen($headerfilePath, "w");
            fwrite($headerfile, $content);
            fclose($headerfile);
             Liyi::$msg .=$headerfilePath.'创建成功！<br />';
            return 1;
      }
        Liyi::$msg .=$headerfilePath.'已经存在！<br />';
      return 2;
       }
       
      
    
}